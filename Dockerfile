FROM ubuntu:bionic

ENV DEBIAN_FRONTEND="noninteractive"
ENV ACTENCY_TOOLTIP="v1.0.0"

# Install a set of tools + Docker
RUN set -eux; \
	apt-get update; apt-get install -y \
        bzip2 \
        curl \
        gcc \
        gnupg dirmngr \
        make \
        bc \
        cpio \
        dpkg-dev \
        g++ \
        patch \
        perl \
        python \
        rsync \
        unzip \
        wget \
        htop \
        git \
        php-cli \
        php-zip \
        nodejs \
        npm \
        vim \
        automake \
        ruby2.5-dev \
        libtool \
        telnet \
        dnsutils \
        rubygems \
        ruby-dev \
        sudo \
        iputils-ping \
        bash-completion \
        jq \
        docker.io \
        ; rm -rf /var/lib/apt/lists

# Installation of docker-compose
RUN cd /usr/src; \
    curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose; \
    chmod +x /usr/local/bin/docker-compose; \
    docker-compose --version

# Installation of composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer; \
    composer --version

# Installation of helm v3.0.3
RUN cd /usr/src; \
    wget https://get.helm.sh/helm-v3.0.3-linux-amd64.tar.gz; \
    tar -zxvf helm-v3.0.3-linux-amd64.tar.gz; \
    mv linux-amd64/helm /usr/local/bin/helm; \
    rm helm-v3.0.3-linux-amd64.tar.gz; \
    rm -rf linux-amd64; \
    helm version

# Installation of kubectl
RUN cd /usr/src; \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl; \
    chmod +x ./kubectl; \
    mv ./kubectl /usr/local/bin/kubectl; \
    echo 'source <(kubectl completion bash)' >>~/.bashrc; \
    kubectl completion bash >/etc/bash_completion.d/kubectl

# Installation of gcloud client
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-sdk -y; rm -rf /var/lib/apt/lists

# Installation of azure client
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

# Installation of aws client
RUN apt-get update && apt-get install -y \
    awscli \
    ; rm -rf /var/lib/apt/lists

# SASS and Compass installation
RUN gem install sass -v 3.5.6 ;\
  gem install compass;

# Installation of LESS
RUN npm install -g less && npm install -g less-plugin-clean-css

# Installation of Grunt
RUN npm install -g grunt-cli

# Installation of Gulp
RUN npm install -g gulp

# Installation of Bower
RUN npm install -g bower

# Installation of drush 8 & 9
RUN git clone https://github.com/drush-ops/drush.git /usr/local/src/drush; \
    cd /usr/local/src/drush && git checkout 9.1.0; \
    composer update && composer install; \
    ln -s /usr/local/src/drush/drush /usr/bin/drush

# Install Drupal Console for Drupal 8
RUN curl https://drupalconsole.com/installer -L -o drupal.phar && mv drupal.phar /usr/local/bin/drupal && chmod +x /usr/local/bin/drupal

CMD ["sh"]
